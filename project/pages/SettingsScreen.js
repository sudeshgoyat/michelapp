// React Native Bottom Navigation
// https://aboutreact.com/react-native-bottom-navigation/
import * as React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  SafeAreaView,Image,Linking 
} from 'react-native'; 
class SettingsScreen extends React.Component   {
  render() {

    const { item } = this.props.route.params.data;
   
     
  return (
    <SafeAreaView style={{ flex: 1,position: 'absolute',
    top: 0,
   
    width: '100%',
    height:300 }}>
    <View style={styles.container}>
     
<TouchableOpacity style={styles.container} onPress={ ()=>  Linking.openURL(item.previewUrl)
             }>
          
         
          <Image 
              source = {{ uri: item.artworkUrl60 }}
 
              style = {styles.imageStyle} />
               <Text   style={styles.textViewContainer} >Track:-{item.trackName}
               {"\n"} Artist Name:- {item.artistName}
               {"\n"} primary gener Name:- {item.primaryGenreName}
               {"\n"} Description:-{item.longDescription}
               </Text>
             
              
               </TouchableOpacity>
    </View>
   
    </SafeAreaView>
  )
        }
}
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex:1,
  //  flexDirection: 'row'
    
   },
   item: {
     padding: 10,
     fontSize: 18,
     height: 44,
   },
   imageStyle:{
    width: '50%',
    height: 100 ,
    margin: 10,
    borderRadius : 10,resizeMode: 'cover'
   },
   textViewContainer: {
 
    textAlignVertical:'center',
    width:'50%', 
    padding:5
   
  },
  textViewContainer1: {
 
    textAlignVertical:'center',
    width:'100%', 
    padding:5
   
  },
});
export default SettingsScreen;