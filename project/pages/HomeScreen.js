import * as React from 'react';
import { useState,useEffect } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,ActivityIndicator,
  SafeAreaView,FlatList,Image,StatusBar
} from 'react-native';

class HomeScreen extends React.Component {
       

  constructor(props){
    super(props);
    this.state={
      data:[]
    }
  }

  componentDidMount(){
    console.log('profile')
   
  
    fetch( 'http://itunes.apple.com/search?term=Michael+jackson')
    .then((response) =>   response.json())
    .then((json) =>  {console.log('jsonn',json); this.setState({data:json.results})})
    .catch((error) => console.error(error))
    // .finally(() => setLoading(false));
  } 
  
  render() {
    const data=this.state.data;
    if (this.state.data.length === 0) {
      return (
        <ActivityIndicator
          animating={true}
          style={styles.indicator}
          size="large"
        />
      );
    }
  return (

    
   
   
<View style={styles.container}>
      <FlatList
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) =>( 

     
          <TouchableOpacity   onPress={
            () => { 
             this.props.navigation.navigate(
              'Settings', 
              { data: { item: item} }
            )}}>
          <Image 
              source = {{ uri: item.artworkUrl30 }}
 
              style = {styles.imageStyle} />
               <Text   style={styles.textViewContainer} >{item.trackName}</Text>
                </TouchableOpacity>
              
        )}
      />
    </View>

    
        
        
     
  );
        }
}

const styles = StyleSheet.create({
  
   
   container: {
    justifyContent: 'center',
    flex:1,
    marginTop: StatusBar.currentHeight || 0,
    
    
   },
   indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80,backgroundColor:'red'
  },
   item: {
     padding: 10,
     fontSize: 18,
     height: 44,
   },
   imageStyle:{
    width: '50%',
    height: 100 ,
    margin: 10,
    borderRadius : 10,resizeMode: 'cover'
   },
   textViewContainer: {
 
    textAlignVertical:'center',
    width:'50%', 
    padding:5
   
  },
});
export default HomeScreen;